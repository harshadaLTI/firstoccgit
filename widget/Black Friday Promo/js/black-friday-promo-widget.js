define (
  //-------------------------------------------------------------------
  // DEPENDENCIES
  //-------------------------------------------------------------------
  [
    'knockout',
    'ccDate',
    'ccNumber',
    'CCi18n'
  ],

  //-------------------------------------------------------------------
  // MODULE DEFINITION
  //-------------------------------------------------------------------
  function (ko, ccDate, ccNumber, CCi18n) {

    "use strict";

    return {

      startDate     : new Date("November, 24, 2016"),
      closingDate   : new Date("November, 28, 2016"),
      remainingDays : ko.observable(5),
      currentDate   : new Date("November, 26, 2016"),
      minSpend      : 250,
      percentageOff : 15.4,

      calculateRemainingDays : function (widget) {

        var daysCount = (widget.closingDate - widget.currentDate) / (1000*60*60*24);
        daysCount = Math.round(daysCount);

        return(daysCount);
      },

      onLoad : function (widget) {

        var calculatedRemainingDays = widget.calculateRemainingDays(widget);
        widget.remainingDays(calculatedRemainingDays);

        widget.minSpend      = ccNumber.formatNumber(widget.minSpend, true);
        widget.percentageOff = ccNumber.formatNumber(widget.percentageOff, undefined, 0) + "%";

        widget.startDate     = ccDate.dateTimeFormatter(widget.startDate, null, 'short');
        widget.closingDate   = ccDate.dateTimeFormatter(widget.closingDate, null, 'short');

        widget.remainingText = CCi18n.t('ns.librariesDemo:resources.blackFridayRemaining', {
          remaining : widget.remainingDays()
        });

        widget.offerText = CCi18n.t('ns.librariesDemo:resources.blackFridayOffer', {
          percentageOff : widget.percentageOff,
          minSpend      : widget.minSpend
        });

        widget.extraSpend = widget.minSpend - widget.cart().total();

      }

    }

  }

);
