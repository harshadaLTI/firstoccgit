/**
 * @fileoverview View Recently Viewed.
 */

define(
  //-------------------------------------------------------------------
  // DEPENDENCIES
  //-------------------------------------------------------------------
  ['knockout', 'pubsub', 'ccRestClient', 'ccLogger', 'storageApi'],

  //-------------------------------------------------------------------
  // MODULE DEFINITION
  //-------------------------------------------------------------------
  function(ko, pubsub, ccRestClient, ccLogger, storageApi) {

    "use strict";

    var self;

    return {

      onLoad: function(widget) {
        self = widget;

        self.recentProducts = ko.observableArray();
      },

      beforeAppear: function(page) {
        self.recentProducts.removeAll();

        var recentlyViewedCookie = JSON.parse(storageApi.getInstance().getItem("cc.product.recentlyViewed")) || [];

        // Refresh the Recently Viewed Data
        if (recentlyViewedCookie.length > 0) {

          var pProductIds = [];

          for (var i = 0; i < recentlyViewedCookie.length; i++) {
            // Check not back on page for this product
            if (recentlyViewedCookie[i].id !== page.contextId) {
              pProductIds.push(recentlyViewedCookie[i].id);
            }
          }

          if (pProductIds.length > self.numDisplayed()) {
            // only fetch data for products that will be displayed
            pProductIds.length = self.numDisplayed();
          }

          var fields = "id,displayName,route,primaryThumbImageURL";

          var pData = {
            productIds: pProductIds,
            fields: fields
          };

          ccRestClient.request('listProducts', pData,
            function (upToDateProductData) { self.updateRecentProductData(upToDateProductData); },
            function (errorData) { ccLogger.warn("Error retrieving recently view products"); }
          );
        }

      },

      updateRecentProductData: function(upToDateProductData) {
        for (var i = 0; i < upToDateProductData.length; i++) {
          self.recentProducts.push(upToDateProductData[i]);
        }
      }
    };
  }
);
