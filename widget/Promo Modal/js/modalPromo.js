/**
 * @fileoverview Sample Widget.
 *
 * @author Your Name Here
 */
define(	
	//-------------------------------------------------------------------
	// DEPENDENCIES
	//-------------------------------------------------------------------
	['jquery','knockout', 'notifier', 'ccPasswordValidator', 'pubsub', 'CCi18n','ccConstants', 'navigation', 'ccLogger'],

	//-------------------------------------------------------------------
	// MODULE DEFINITION
	//-------------------------------------------------------------------
	function ($,ko, notifier, CCPasswordValidator, pubsub, CCi18n, CCConstants,
navigation, ccLogger) {

    "use strict";
    //var widget;

	
	
    return {
	    
		
		showErrorMessage:ko.observable(false),
		ignoreBlur:ko.observable(false),
// 		readCookie: function (name) {
//                 var nameEQ = name + "=";
//                 var ca = document.cookie.split(';');
//                 for (var i = 0; i < ca.length; i++) {
//                     var c = ca[i];
//                     while (c.charAt(0) == ' ') c = c.substring(1, c.length);
//                     if (c.indexOf(nameEQ) === 0) {
//                         var str = unescape(c.substring(nameEQ.length, c.length));
//                         return (str.substring(1, str.length - 2));
//                     }
//                 }
//                 return null;
//             },
//                  getAdminToken: function () {
//                 var adminToken = sessionStorage.getItem('oauth_token_secret-adminUI');
//                 if (adminToken === null || admintToken === '')
//                     adminToken = widget.readCookie('oauth_token_secret-adminUI');
//                 return adminToken;
//             },
		
		onLoad: function(widget) {	
		    
			var self=this;
			

        


			$(document).on('hide.bs.modal','#CC-modalpane', function () {
			  if ($('#CC-UserPane').css('display') == 'block') {
				$('#alert-modal-change').text(CCi18n.t('ns.common:resources.registrationModalClosedText'));
			  }
			  

			 
			});
			
			$(document).on('show.bs.modal','#CC-modalpane', function () {
			  if ($('#CC-UserPane').css('display') == 'block') {
			   $('#alert-modal-change').text(CCi18n.t('ns.common:resources.registrationModalOpenedText'));
			  }
			  
          
			});
	
		
		},
		
		
		


		
		cancelRegistration: function() {
        
                 this.hideLoginModal();
       
                 this.showErrorMessage(false);
                 this.user().pageToRedirect(null);
                },   

      /**
       * Invoked when registration link is clicked
       */
      clickRegistration: function() {
        
        
        this.hideAllSections();
        $('#CC-UserPane').show();
        this.showErrorMessage(false);
        
      },
	  
	  hideAllSections: function() {
        
        $('#CC-UserPane').hide();
	
        
      },
	  /**
       * Ignores the blur function when mouse click is up
       */
      handleMouseUp: function(data) {
        this.ignoreBlur(false);
        
        return true;
      },

      /**
       * Ignores the blur function when mouse click is down
       */
      handleMouseDown: function(data) {
        this.ignoreBlur(true);
        
        return true;
      },
	  /**
       * this method is invoked to hide the login modal
       */
      hideLoginModal: function() {
        $('#CC-modalpane').modal('hide');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
      },
	  searchHandler: function() {
					   var self=this; 
						var dataObject={"enabled":true};
				$.ajax({
              'url':'https://ccadmin-z98a.oracleoutsourcing.com/ccadmin/v1/promotions/promo40006',
              'method': 'PUT',
              
             
              'contentType': 'application/json',
              
              'headers': {
                // Use access_token previously retrieved from login token 
                // service.
				
                'Authorization': 'Bearer ' + self.user().client().tokenSecret,
                'X-CCAsset-Language':'en'
               
                              },
			  'data': JSON.stringify(dataObject),
			 
              'success': function (result) {
                //Process success actions
                var returnResult = JSON.stringify(result);
                alert('Success!\r\n' + returnResult);
                document.getElementById('callResults').innerHTML = returnResult;
                return result;
              },
              'error': function (XMLHttpRequest, textStatus, errorThrown) {
                //Process error actions
                alert('Error: ' + errorThrown);
                console.log(XMLHttpRequest.status + ' ' + 
                    XMLHttpRequest.statusText);
                return false;
              }
          });
	   },
	
	
	

	  
		beforeAppear:function(page){
		  var widget = this;	
          	  
		 
		}
    };
  }
);
