define(
    ['jquery', 'knockout', 'pubsub', 'koValidate', 'ccKoValidateRules', 'notifications', 'CCi18n', 'ccConstants', 'navigation', 'https://www.google.com/recaptcha/api.js'],
    function ($, ko, pubsub, koValidate, rules) {
        "use strict";

        var $window = $(window),
            locales = ['en', 'de', 'fr', 'es'];
        //test

        return {
            availableCountries: ko.observableArray(['Afghanistan', 'Albania', 'Algeria', 'American Samoa', 'Andorra', 'Angola', 'Antigua', 'Antigua and Barbuda', 'Argentina', 'Armenia', 'Aruba', 'Australia', 'Austria', 'Azerbaijan', 'Bahamas', 'Bahrain', 'Bangladesh', 'Barbados', 'Belarus', 'Belgium',
                'Belize', 'Benin', 'Bermuda', 'Bhutan', 'Bolivia', 'Bosnia and Herzegovina', 'Botswana', 'Brazil', 'Brunei', 'Bulgaria', 'Burkina Faso', 'Burma Myanmar', 'Cambodia', 'Cameroon', 'Canada', 'Cape Verde', 'Central African Republic', 'Chad', 'Chile',
                'People\'s Republic of China', 'Republic of China (Taiwan)', 'Cook Islands', 'Colombia', 'Comoros', 'Democratic Republic of the Congo (formerly Zaire)', 'Republic of the Congo', 'Costa Rica', 'Croatia', 'Cuba', 'Republic of Cyprus', 'Czech Republic',
                'Denmark', 'Djibouti', 'Dominica', 'Dominican Republic', 'East Timor', 'Ecuador', 'Egypt', 'El Salvador', 'Equatorial Guinea', 'Eritrea', 'Estonia', 'Ethiopia', 'Fiji', 'Finland', 'France', 'Faroe Islands', 'Gabon', 'The Gambia', 'Georgia', 'Germany',
                'Ghana', 'Greece', 'Grenada', 'Guatemala', 'Guinea', 'Guinea-Bissau', 'Guyana', 'Haiti', 'Holy See (see Vatican City)', 'Honduras', 'Hungary', 'Iceland', 'India', 'Indonesia', 'Iran', 'Iraq', 'Ireland', 'Israel', 'Italy', 'Ivory Coast (see Côte d\'Ivoire)',
                'Jamaica', 'Japan', 'Jordan', 'Kazakhstan', 'Kenya', 'Kiribati', 'Kuwait', 'Kyrgyzstan', 'Laos', 'Latvia', 'Lebanon', 'Lesotho', 'Liberia', 'Libya', 'Liechtenstein', 'Lithuania', 'Luxembourg', 'Republic of Macedonia', 'Madagascar', 'Malawi', 'Malaysia',
                'Maldives', 'Mali', 'Malta', 'Marshall Islands', 'Mauritania', 'Mauritius', 'Mexico', 'Federated States of Micronesia', 'Moldova', 'Monaco', 'Mongolia', 'Montenegro', 'Morocco', 'Mozambique', 'Myanmar', 'Namibia', 'Nauru', 'Nepal', 'Netherlands', 'New Zealand (Aotearoa)',
                'Nicaragua', 'Niger', 'Nigeria', 'Niue', 'North Korea', 'Norway', 'Oman', 'Pakistan', 'Palau', 'Palestine', 'Panama', 'Papua New Guinea', 'Paraguay', 'Peru', 'Philippines', 'Poland', 'Portugal', 'Qatar', 'Romania', 'Russia', 'Rwanda', 'Saint Kitts and Nevis',
                'Saint Lucia', 'Saint Vincent and the Grenadines', 'Samoa', 'San Marino', 'Saudi Arabia', 'Senegal', 'Serbia', 'Seychelles', 'Sierra Leone', 'Singapore', 'Slovakia', 'Slovenia', 'Solomon Islands', 'Somalia', 'South Africa', 'South Korea', 'South Sudan', 'Spain',
                'Sri Lanka', 'Sudan', 'Suriname', 'Swaziland', 'Sweden', 'Switzerland', 'Syria', 'Taiwan (see Republic of China)', 'Tajikistan', 'Tanzania', 'Thailand', 'Togo', 'Tonga', 'Trinidad and Tobago', 'Tunisia', 'Turkey', 'Turkmenistan', 'Tuvalu',
                'Uganda', 'Ukraine', 'United Arab Emirates', 'United Kingdom', 'United States', 'Uruguay', 'Uzbekistan', 'Vanuatu', 'Vatican City (Holy See)', 'Venezuela', 'Vietnam', 'Yemen', 'Zaire (now Democratic Republic of the Congo)', 'Zambia', 'Zimbabwe', 'Åland']),
            
            isSubmitEnabled: ko.observable(false),
            isFormValid: ko.observable(false),
           
            selectedCountry: ko.observable("United States"),
            firstname: ko.observable(""),
            lastname: ko.observable(""),
            postcode: ko.observable(""),
            email: ko.observable(""),
            phone: ko.observable(""),
            company: ko.observable(""),
            address1: ko.observable(""),
            address2: ko.observable(""),
            city: ko.observable(""),
            state: ko.observable(""),
            
            countryText: ko.observable(""),
            firstnameText: ko.observable(""),
            lastnameText: ko.observable(""),
            address1Text: ko.observable(""),
            address2Text: ko.observable(""),
            cityText: ko.observable(""),
            stateText: ko.observable(""),
            postCodeText: ko.observable(""),
            emailText: ko.observable(""),
            phoneText: ko.observable(""),
            companyText: ko.observable(""),
           
            submitText: ko.observable(""),
            cancelText: ko.observable(""),
            
            getLocale: function () {
                var self = this;
                
                var currentLocale = '';
                
                locales.forEach(function(locale) {
                    if (self.localeResources.hasOwnProperty(locale)) {
                        currentLocale = locale;
                    }
                });
                
                return currentLocale;
            },

            onLoad: function (widget) {
                var self = widget;

                

                self.countryText(self.translate('countryText'));
                self.firstnameText(self.translate('firstnameText'));
                self.lastnameText(self.translate('lastnameText'));
                self.address1Text(self.translate('address1Text'));
                self.address2Text(self.translate('address2Text'));
                self.cityText(self.translate('cityText'));
                self.stateText(self.translate('stateText'));
                self.postCodeText(self.translate('postCodeText'));
                self.phoneText(self.translate('phoneText'));
                self.emailText(self.translate('emailText'));
                self.companyText(self.translate('companyText'));
              
                self.submitText(self.translate('submitText'));
                self.cancelText(self.translate('cancelText'));

                self.userValidatableFields = ko.validatedObservable({
                    firstname: self.firstname.extend({ required: {
                    params: true,
                    message: self.translate('firstNameRequiredText')}
                    }),
                    lastname: self.lastname.extend({required: true}),
                    postcode: self.postcode.extend({
                        required: true,
                        number: true
                    }),
                    email: self.email.extend({
                        required: true,
                        email: true
                    }),
                    phone: self.phone.extend({
                        required: true,
                        pattern: {
                            message: 'Invalid phone number.',
                            params: /^\+?\d*$/
                        }
                    }),
                    company: self.company.extend({required:{params:true,message: self.translate('companyNameRequiredText')}
                        
                    })
                });

                self.showFiredUserFieldsValidationMessages = function(){
                    self.userValidatableFields().firstname.valueHasMutated();
                    self.userValidatableFields().lastname.valueHasMutated();
                    self.userValidatableFields().postcode.valueHasMutated();
                    self.userValidatableFields().email.valueHasMutated();
                    self.userValidatableFields().phone.valueHasMutated();
                    self.userValidatableFields().company.valueHasMutated();
                };

                self.validateField = function(data, event){
                    self.userValidatableFields()[event.target.id].valueHasMutated();
                    
                    var errors = koValidate.group(this, {
                        observable: false
                    })();
                    self.isFormValid(errors.length === 0);
                    
                };
            },

            handleNewAccountRequest: function () {
                
                var self = this;

                self.showFiredUserFieldsValidationMessages();

               
                console.log(self.selectedCountry());
                console.log(self.firstname());
                console.log(self.lastname());
                console.log(self.postcode());
                console.log(self.email());
                console.log(self.address1());
                console.log(self.address2());
                console.log(self.city());
                console.log(self.state());
                console.log(self.phone());
                console.log(self.company());
                
            },


            beforeAppear: function (page) {
                var self = this;

                // fix for platform bug - beforeAppear is called when it should not
                if (this.pageIds().indexOf(page.pageRepositoryId) === -1)
                    return;

                

                

                

            },

           
            

            submit: function () {
                var self = this;

                window.location.href = '/';
            },
            
            
            
            exit: function () {
                var self = this;
                
                var locale = self.getLocale();
                window.location.href = '/' + locale;
            }
        };
    }
);