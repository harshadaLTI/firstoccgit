define(
  //-------------------------------------------------------------------
  // DEPENDENCIES
  //-------------------------------------------------------------------
  ['jquery', 'knockout', 'notifier', 'ccPasswordValidator', 'pubsub', 'CCi18n', 
   'ccConstants', 'navigation', 'ccLogger'],
    
  //-------------------------------------------------------------------
  // MODULE DEFINITION
  //-------------------------------------------------------------------
  function ($, ko, notifier, CCPasswordValidator, pubsub, CCi18n, CCConstants,
    navigation, ccLogger) {
    "use strict";

    return {
      

      
      showErrorMessage:   ko.observable(false),
      userCreated:        ko.observable(false),
      ignoreBlur:         ko.observable(false),
      
      onLoad: function(widget) {
        var self = this;

        widget.user().ignoreEmailValidation(false);
        // To display success notification after redirection from customerProfile page.
        if(widget.user().delaySuccessNotification()) {
          notifier.sendSuccess(widget.WIDGET_ID, widget.translate('updateSuccessMsg'), true);
          widget.user().delaySuccessNotification(false);
        }

        // Handle widget responses when registration is successful or invalid
        $.Topic(pubsub.topicNames.USER_AUTO_LOGIN_SUCCESSFUL).subscribe(function(obj) {
          if(obj.widgetId === widget.WIDGET_ID) {
            self.userCreated(true);

            self.showErrorMessage(false);
            notifier.clearSuccess(widget.WIDGET_ID);
            notifier.sendSuccess(widget.WIDGET_ID, widget.translate('createAccountSuccess') );
            $(window).scrollTop('0');
          }
        });

        
        $.Topic(pubsub.topicNames.USER_PASSWORD_GENERATED).subscribe(function(data) {
          $('#alert-modal-change').text(CCi18n.t('ns.common:resources.resetPasswordModalOpenedText'));
          widget.user().ignoreEmailValidation(false);
          
         
          widget.user().emailAddressForForgottenPwd('');
          widget.user().emailAddressForForgottenPwd.isModified(false);
        });
        
        
        

        $.Topic(pubsub.topicNames.USER_CREATION_FAILURE).subscribe(function(obj) {
          if(obj.widgetId === widget.WIDGET_ID) {
            widget.user().resetPassword();
            
            self.showErrorMessage(true);
          };
        });
        
        $.Topic(pubsub.topicNames.USER_LOGIN_FAILURE).subscribe(function(obj) {
          self.modalMessageType("error");

          if (obj.errorCode && obj.errorCode === CCConstants.ACCOUNT_ACCESS_ERROR_CODE) {
            self.modalMessageText(CCi18n.t('ns.common:resources.accountError'));
          }
          else {
            self.modalMessageText(CCi18n.t('ns.common:resources.loginError'));
          }

          self.showErrorMessage(true);
        });
        
        $.Topic(pubsub.topicNames.USER_LOGIN_SUCCESSFUL).subscribe(function(obj) {
          self.hideLoginModal();
          self.showErrorMessage(false);
          notifier.clearSuccess(widget.WIDGET_ID);
          
        });
        
        // Replacing pubsub subscription with this. PubSub's getting called multiple times, causing this method to be called multiple times,
        // causing login modal to appear and disappears at times.
         

        // This pubsub checks for the page parameters and if there is a token
        // in the page URL, validates it and then starts the update expired/
        // forgotten password modal.
        

       

      },

      removeMessageFromPanel: function() {
        var message = this;
        var messageId = message.id();
        var messageType = message.type();
        notifier.deleteMessage(messageId, messageType);
      },

      emailAddressFocused : function(data) {
        if(this.ignoreBlur && this.ignoreBlur()) {
          return true;
        }
        this.user().ignoreEmailValidation(true);
        return true;
      },

      emailAddressLostFocus : function(data) {
        if(this.ignoreBlur && this.ignoreBlur()) {
          return true;
        }
        this.user().ignoreEmailValidation(false);
        return true;
      },

      passwordFieldFocused : function(data) {
        if(this.ignoreBlur && this.ignoreBlur()) {
          return true;
        }
        this.user().ignorePasswordValidation(true);
        return true;
      },

      passwordFieldLostFocus : function(data) {
        if(this.ignoreBlur && this.ignoreBlur()) {
          return true;
        }
        this.user().ignorePasswordValidation(false);
        return true;
      },

      confirmPwdFieldFocused : function(data) {
        if(this.ignoreBlur && this.ignoreBlur()) {
          return true;
        }
        this.user().ignoreConfirmPasswordValidation(true);
        return true;
      },

      confirmPwdFieldLostFocus : function(data) {
        if(this.ignoreBlur && this.ignoreBlur()) {
          return true;
        }
        this.user().ignoreConfirmPasswordValidation(false);
        return true;
      },

      
      /**
       * Registration will be called when register is clicked
       */
      registerUser: function(data, event) {
        if ('click' === event.type || (('keydown' === event.type || 'keypress' === event.type) && event.keyCode === 13)) {
          notifier.clearError(this.WIDGET_ID);
          //removing the shipping address if anything is set
          data.user().shippingAddressBook([]);
          data.user().updateLocalData(false, false);
          if (data.user().validateUser()) {
            $.Topic(pubsub.topicNames.USER_REGISTRATION_SUBMIT).publishWith(data.user(), [{message: "success", widgetId: data.WIDGET_ID}]);
          }
        }
        return true;
      }

      /**
       * this method is invoked to hide the login modal
       */
      

      /**
       * Invoked when Login method is called
       */
       

      /**
       * Invoked when cancel button is clicked on login modal
       */
       
      
      

      /**
       * this method is triggered when the user clicks on the save 
       * on the update password model
       */
      

      /**
       * Invoked when cancel button is called on 
       */
      

      /**
       * Invoked when Logout method is called
       */
      

      /**
       * Invoked when the modal dialog for registration is closed
       */
     

      /**
       * Invoked when registration link is clicked
       */
      

      /**
       * Invoked when login link is clicked
       */
      

      /**
       * Ignores the blur function when mouse click is up
       */
      

      /**
       * Ignores the blur function when mouse click is down
       */
      

      /**
       * Ignores the blur function when mouse click is down outside the modal dialog(backdrop click).
       */
      

     /**
      * Invoked when register is clicked on login modal
      */
      

      /**
       * Invoked when forgotten Password link is clicked.
      

      /**
       * Hides all the sections of  modal dialogs.
       */
      

      /**
       * Resets the password for the entered email id.
       */
      
    };
  }
);
