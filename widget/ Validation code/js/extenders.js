/**
 * @fileoverview Sample Widget.
 *
 * @author Your Name Here
 */
define(	
	//-------------------------------------------------------------------
	// DEPENDENCIES
	//-------------------------------------------------------------------
	['knockout'],

	//-------------------------------------------------------------------
	// MODULE DEFINITION
	//-------------------------------------------------------------------
	function (ko) {
		ko.extenders.required = function(target, overrideMessage) {
		//add some sub-observables to our observable
		target.hasError = ko.observable();
		target.validationMessage = ko.observable();
 
		//define a function to do validation
    function validate(newValue) {
       target.hasError(newValue ? false : true);
       target.validationMessage(newValue ? "" : overrideMessage || "This field is required");
    }
 
    //initial validation
    validate(target());
 
    //validate whenever the value changes
    target.subscribe(validate);
 
    //return the original observable
    return target;
};
    
    return {
	    
		firstName:ko.observable("").extend({ required: "Please enter a first name" }),
		lastName: ko.observable("").extend({ required: "Please enter a last name" }),
		someValue: ko.observable(""),
		

		onLoad: function(widget) {	
         var self=this;
         
            widget.showMessage = function(data,event) {
            
               if ((event.keyCode < 47) || (event.keyCode > 58 )) {  //check for number
                  if (event.keyCode !== 8)   //ignore backspace
                  alert("Please enter a number.");
                  this.someValue('');
               }
            }

        
		
		},
		beforeAppear:function(page){
			var widget = this;	
		  
		}
    };
  }
);
