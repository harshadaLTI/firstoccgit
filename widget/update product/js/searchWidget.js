define(
// DEPENDENCIES
['jquery','knockout', 'ccConstants', 'ccRestClient', 'notifier'],
// MODULE DEFINITION
  function ($,ko, CCConstants, ccRestClient, notifier) {
				"use strict";
				var widget;
              return {
                  
               readCookie: function (name) {
                var nameEQ = name + "=";
                var ca = document.cookie.split(';');
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                    if (c.indexOf(nameEQ) === 0) {
                        var str = unescape(c.substring(nameEQ.length, c.length));
                        return (str.substring(1, str.length - 2));
                    }
                }
                return null;
            },
                 getAdminToken: function () {
                var adminToken = sessionStorage.getItem('oauth_token_secret-adminUI');
                if (adminToken === null || admintToken === '')
                    adminToken = widget.readCookie('oauth_token_secret-adminUI');
                return adminToken;
            },
					
					onLoad: function(widgetModel) {
					   widget=widgetModel;
					},
				
				
					beforeAppear: function(page) {
						
					},
					
					
					searchHandler: function() {
						var self = this;
					    
						var dataObject={"shoppingCart":{"items":[{"productId":"10-11-9299","quantity":1,"repositoryId":"ci10009668","availabilityDate":null,"catRefId":"Airfil101","expanded":false,"stockStatus":true,"stockState":"IN_STOCK","orderableQuantityMessage":"","commerceItemQuantity":1,"orderableQuantity":30,"selectedOptions":[],"selectedSkuProperties":[],"discountInfo":[],"rawTotalPrice":180.25,"detailedItemPriceInfo":[{"discounted":false,"secondaryCurrencyTaxAmount":0,"amount":180.25,"quantity":1,"tax":0,"orderDiscountShare":0,"detailedUnitPrice":180.25,"currencyCode":"USD"}],"externalData":[],"addOnItem":false,"shopperInput":{},"displayName":"","childItems":null,"invalid":false,"commerceItemId":"ci10009638","priceListGroupId":"defaultPriceGroup","giftWithPurchaseCommerceItemMarkers":[],"price":180.25},{"productId":"10-11-9059","quantity":1,"repositoryId":"ci10009669","availabilityDate":null,"catRefId":"Airfilt102","expanded":false,"stockStatus":true,"stockState":"IN_STOCK","orderableQuantityMessage":"","commerceItemQuantity":1,"orderableQuantity":19,"selectedOptions":[],"discountInfo":[],"rawTotalPrice":10.85,"detailedItemPriceInfo":[{"discounted":false,"secondaryCurrencyTaxAmount":0,"amount":10.85,"quantity":1,"tax":0,"orderDiscountShare":0,"detailedUnitPrice":10.85,"currencyCode":"USD"}],"externalData":[],"addOnItem":false,"displayName":"AIR FILTER (3.74/3.95)","invalid":false,"commerceItemId":"ci10009669","priceListGroupId":"defaultPriceGroup","giftWithPurchaseCommerceItemMarkers":[],"price":10.85}],"coupons":[]},"combineLineItems":"yes","payments":[],"shipping_options":null,"shippingMethod":{"value":"200019"},"shippingAddress":{"alias":"Address","prefix":"","firstName":"Lucas","middleName":"","lastName":"Martin","suffix":"","country":"Canada","postalCode":"M5V 3T4","address1":"318 Wellington Street West","address2":"","address3":"","city":"Toronto","state":"Ontario","county":"","phoneNumber":"","email":"testaccount1@gmail.com","jobTitle":"","companyName":"","faxNumber":"","repositoryId":"736852","isDefaultBillingAddress":false,"isDefaultShippingAddress":false,"selectedCountry":"CA","selectedState":"ON","state_ISOCode":"CA-ON","isDefaultAddress":true},"populateShippingMethods":true,"checkout":true,"clearCoupons":false,"merge":false,"op":"update"};
				$.ajax({
              'url':'https://ccadmin-z98a.oracleoutsourcing.com/ccstoreui/v1/orders/current',
              'method': 'POST',
              
             
              'contentType': 'application/json',
              
              'headers': {
                // Use access_token previously retrieved from login token 
                // service.
				
                'Authorization': 'Bearer ' +self.user().client().tokenSecret,
                'X-CCAsset-Language':'en'
               
                              },
			  'data': JSON.stringify(dataObject),
			 
              'success': function (result) {
                //Process success actions
                var returnResult = JSON.stringify(result);
                alert('Success!\r\n' + returnResult);
                document.getElementById('callResults').innerHTML = returnResult;
                return result;
              },
              'error': function (XMLHttpRequest, textStatus, errorThrown) {
                //Process error actions
                alert('Error: ' + errorThrown);
                console.log(XMLHttpRequest.status + ' ' + 
                    XMLHttpRequest.statusText);
                return false;
              }
          });
				// 	var pathParam = "prodex103";
				// 	var data={"properties":{"salePrice":350}};
				// 		//	var data = {items:[{}]};
				// 		//	data.items[0].
				// 		  // var url=CCConstants.ENDPOINT_PRODUCTS_UPDATE_PRODUCT;
				// 		  // var url=CCConstants.ENDPOINT_PRODUCT_UPDATE_VARIANT_SKU;
				// 		  //var url=CCConstants.ENDPOINT_UPDATE_PROFILE;
				// 		  //data[CCConstants.PROFILE_FIRST_NAME]="Unnati";
				// 			var url=CCConstants.ENDPOINT_PRODUCTS_UPDATE_PRODUCT;
				// 		//	perform search
				// 			ccRestClient.request(url, data, widget.successCallback.bind(widget),
				// 			widget.errorCallback.bind(widget),pathParam);
					
				// 	},
					
				// 	successCallback: function(result) {
				// 		console.log('response',result);
				// 	},
					
				// 	errorCallback: function(data) {
				// 		 console.log('data errror.....',data);
					}
				};
	}
);