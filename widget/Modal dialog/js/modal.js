/**
 * @fileoverview Sample Widget.
 *
 * @author Your Name Here
 */
define(	
	//-------------------------------------------------------------------
	// DEPENDENCIES
	//-------------------------------------------------------------------
	['jquery','knockout', 'notifier', 'ccPasswordValidator', 'pubsub', 'CCi18n','ccConstants', 'navigation', 'ccLogger'],

	//-------------------------------------------------------------------
	// MODULE DEFINITION
	//-------------------------------------------------------------------
	function ($,ko, notifier, CCPasswordValidator, pubsub, CCi18n, CCConstants,
navigation, ccLogger) {

    "use strict";

	
	
    return {
	    
		
		showErrorMessage:ko.observable(false),
		ignoreBlur:ko.observable(false),
		
		onLoad: function(widget) {	
			var self=this;
			widget.user().ignoreEmailValidation(false);
			$.Topic(pubsub.topicNames.USER_PASSWORD_GENERATED).subscribe(function(data) {
         		 	$('#alert-modal-change').text(CCi18n.t('ns.common:resources.resetPasswordModalOpenedText'));
         			 widget.user().ignoreEmailValidation(false);
         			 self.hideAllSections();
        		 	 $('#CC-forgotPasswordSectionPane').show();
         		 	$('#CC-forgotPwd-input').focus();
          			widget.user().emailAddressForForgottenPwd('');
        	  		widget.user().emailAddressForForgottenPwd.isModified(false);
       		 	});
			
			$.Topic(pubsub.topicNames.USER_RESET_PASSWORD_SUCCESS).subscribe(function(data) {
         			 self.hideAllSections();
          			self.hideLoginModal();
         		 notifier.sendSuccess(widget.WIDGET_ID, CCi18n.t('ns.common:resources.resetPasswordMessage'), true);
        		});

        


			$(document).on('hide.bs.modal','#CC-modalpane', function () {
			  if ($('#CC-UserPane').css('display') == 'block') {
				$('#alert-modal-change').text(CCi18n.t('ns.common:resources.registrationModalClosedText'));
			  }
			  else if ($('#CC-forgotPasswordSectionPane').css('display') == 'block') {
           			 $('#alert-modal-change').text(CCi18n.t('ns.common:resources.resetPasswordModalClosedText'));
          		  }

			 
			});
			
			$(document).on('show.bs.modal','#CC-modalpane', function () {
			  if ($('#CC-UserPane').css('display') == 'block') {
			   $('#alert-modal-change').text(CCi18n.t('ns.common:resources.registrationModalOpenedText'));
			  }
			  else if ($('#CC-forgotPasswordSectionPane').css('display') == 'block') {
           			 $('#alert-modal-change').text(CCi18n.t('ns.common:resources.resetPasswordModalOpenedText'));
         		  }

          
			});
	
		
		},
		
		
		emailAddressFocused : function(data) {
        		if(this.ignoreBlur && this.ignoreBlur()) {
         	 	return true;
       		 }
       		 this.user().ignoreEmailValidation(true);
        		return true;
     		 },

     		 emailAddressLostFocus : function(data) {
        		if(this.ignoreBlur && this.ignoreBlur()) {
         		 return true;
       		 }
        	this.user().ignoreEmailValidation(false);
        		return true;
      		},


		
		cancelRegistration: function() {
        
                 this.hideLoginModal();
       
                 this.showErrorMessage(false);
                 this.user().pageToRedirect(null);
                },   

      /**
       * Invoked when registration link is clicked
       */
      clickRegistration: function() {
        
        
        this.hideAllSections();
        $('#CC-UserPane').show();
        this.showErrorMessage(false);
        
      },
	  
	  hideAllSections: function() {
        
        $('#CC-UserPane').hide();
	$('#CC-forgotPasswordSectionPane').hide();
	$('#CC-forgotPasswordMessagePane').hide();
        
      },
	  /**
       * Ignores the blur function when mouse click is up
       */
      handleMouseUp: function(data) {
        this.ignoreBlur(false);
        
        return true;
      },

      /**
       * Ignores the blur function when mouse click is down
       */
      handleMouseDown: function(data) {
        this.ignoreBlur(true);
        
        return true;
      },
	  /**
       * this method is invoked to hide the login modal
       */
      hideLoginModal: function() {
        $('#CC-modalpane').modal('hide');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
      },
	handleCancelForgottenPassword: function(data, event) {
        if('click' === event.type || (('keydown' === event.type || 'keypress' === event.type) && event.keyCode === 13)) {
          notifier.clearError(this.WIDGET_ID);
          navigation.doLogin(navigation.getPath(), data.links().home.route);
        }
        return true;
      },

	  
	showForgotPasswordSection: function() {
        $('#alert-modal-change').text(CCi18n.t('ns.common:resources.forgottenPasswordModalOpenedText'));
        this.user().ignoreEmailValidation(false);
        this.hideAllSections();
        $('#CC-forgotPasswordSectionPane').show();
        $('#CC-forgotPwd-input').focus();
        this.user().emailAddressForForgottenPwd('');
        this.user().emailAddressForForgottenPwd.isModified(false);
      },
	resetForgotPassword: function(data,event) {
        if('click' === event.type || (('keydown' === event.type || 'keypress' === event.type) && event.keyCode === 13)) {
          data.user().ignoreEmailValidation(false);
          data.user().emailAddressForForgottenPwd.isModified(true);
          if(data.user().emailAddressForForgottenPwd  && data.user().emailAddressForForgottenPwd.isValid()) {
            data.user().resetForgotPassword();
          }
        }
        return true;
      },

	  
		beforeAppear:function(page){
		  var widget = this;	
          	  
		 
		}
    };
  }
);
