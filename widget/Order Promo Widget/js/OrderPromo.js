	define(
	
	
	// DEPENDENCIES
	
	['jquery','knockout','ccConstants','pageLayout/product'], 
	
	// MODULE DEFINITION
	
	function ($, ko, CCConstants, product) {	    
	  
		"use strict";
	  
		return {	
		  userStatus: ko.observable(""),
		  onLoad: function(widget) {
				 
		  var self = this;
		  var accessToken = '';
		  
		  widget.customSettings = ko.observableArray([]);
	      var customSettings = widget.site().extensionSiteSettings.customSettings;

	        if(customSettings) {
	          for (var settingKey in customSettings) {
	            var prop = customSettings[settingKey];
	            widget.customSettings.push({
	              'label':settingKey,
	              'value':prop
	            });
	          }
	        }
 
          // The auth_token is the base64 encoded string for the API 
          // application.
          var auth_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJmZTcwMTAxYy04OGViLTQ2ZmQtOTJlNy1hN2ZhMGRjMTk4YTgiLCJpc3MiOiJhcHBsaWNhdGlvbkF1dGgiLCJleHAiOjE1NTc0Njc2MjksImlhdCI6MTUyNTkzMTYyOX0=.UKTLHaPjwHlF/JY+tbRVX8NGW1ujf2y7L4UdC7wiDdU=';
         // auth_token = window.btoa(auth_token);
          var requestPayload = {
              // Enter your  credentials for the 'username' and 
              // 'password' fields.
              'grant_type': 'password',
              'username': 'unnati.barot@lntinfotech.com',
              'password': 'System@123',
              'totp_code': '123456'
          }
          $.ajax({
              'url': 'https://ccadmin-z98a.oracleoutsourcing.com/ccadmin/v1/mfalogin',
			  'async':false,
              'type': 'POST',
              'content-Type': 'x-www-form-urlencoded',
              'dataType': 'json',
              'headers': {
                // Use access_token previously retrieved from inContact token 
                // service.
                'Authorization': 'Bearer' + auth_token
              },
              'data': requestPayload,
              'success': function (result) {
               //Process success actions
               accessToken = result.access_token;
                
               
			$.ajax({
              'url':'https://ccadmin-z98a.oracleoutsourcing.com/ccadmin/v1/products/' + widget.product().id(),
              'type': 'GET',
              'content-Type': 'application/json',
              'dataType': 'json',
              'headers': {
                // Use access_token previously retrieved from login token 
                // service.
                'Authorization': 'Bearer ' +accessToken
              },
              'success': function (result) {
                //Process success actions
                var returnResult = JSON.stringify(result);
                
                document.getElementById('callResults').innerHTML = result.reviews;
                return result;
              },
              'error': function (XMLHttpRequest, textStatus, errorThrown) {
                //Process error actions
                alert('Error: ' + errorThrown);
                console.log(XMLHttpRequest.status + ' ' + 
                    XMLHttpRequest.statusText);
                return false;
              }
          });
                return result;
              },
              'error': function (XMLHttpRequest, textStatus, errorThrown) {
                //Process error actions
                alert('Error: ' + errorThrown);
                console.log(XMLHttpRequest.status + ' ' + 
                    XMLHttpRequest.statusText);
                return false;
              }
          }); 			
			 
			},
			 	beforeAppear:function(page){
				var widget = this;	
				
			},
		};
		}
	  
	);
