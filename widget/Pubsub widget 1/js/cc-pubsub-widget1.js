define(
    ["knockout", "jquery", "pubsub"],
    function(ko, $, pubsub) {
        "use strict";
        return {
			resultMessage1: ko.observable(""),
			// Called when widget loads for first time only
			onLoad: function(widget) {
				console.log("Widget1 called onLoad");
				// Subscribe to widget2 topic
				$.Topic("CUSTOM_TOPIC_2").subscribe(widget.recieveMessageFrom2.bind(widget));
			},
            // Called each time widget appears
			beforeAppear: function(page) {
				console.log("Widget1 called beforeAppear");
			},
			// Called when subsrcibed event fires
			recieveMessageFrom2: function(data){
				this.resultMessage1(data.message);
			},
			// Called when user clicks button
            publishEventFrom1: function(){
				console.log("validate widget 2 data here before checkout");
				var data = {message: "recieved from widget 1"};
				$.Topic("CUSTOM_TOPIC_1").publishWith(data, [{message: "validate widget 2"}]);
			}
		};
	}
);