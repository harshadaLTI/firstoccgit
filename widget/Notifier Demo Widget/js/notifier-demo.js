define(

  //-------------------------------------------------------------------
  // DEPENDENCIES
  //-------------------------------------------------------------------
  ['jquery', 'knockout', 'ccLogger', 'koValidate', 'notifier', 'navigation', 'notifications'],

  //-------------------------------------------------------------------
  // Module definition
  //-------------------------------------------------------------------
  function ($, ko, ccLogger, koValidate, notifier, navigation, notifications) {

    'use strict';

    return {
      onLoad: function (widget) {

        widget.notificationId = ko.observable().extend({
          required: true
        });

        widget.notificationMessage = ko.observable().extend({
          required: true
        });

        widget.scroll = ko.observable(false);
        widget.dismissable = ko.observable(true);
        widget.targetPage = ko.observable();
        widget.targetPath = ko.observable();

        /**
         * Calls the underlying notifier library to
         * send notification messages to the notifications widget.
         *
         * @param {String} type The type of message e.g. info, error, warning, success
         */
        function sendMessage(type) {

          if (widget.notificationId() && widget.notificationMessage()) {
            notifier.sendMessage(
              widget.notificationId(),
              widget.notificationMessage(),
              type,
              widget.dismissable(),
              widget.scroll()
            );
          }
        }

        function clearMessage(type) {
          notifier.clearMessage(widget.notificationId(), type);
        }

        // Wrapper methods to notification library to send messages
        widget.sendSuccess = function () {
          sendMessage(notifier.types.SUCCESS);
        };

        widget.sendError = function () {
          sendMessage(notifier.types.ERROR);
        };

        widget.sendWarning = function () {
          sendMessage(notifier.types.WARNING);
        };

        widget.sendInfo = function () {
          sendMessage(notifier.types.INFO);
        };

        // Wrapper methods to notification library to clear messages
        widget.clearSuccess = function () {
          clearMessage(notifier.types.SUCCESS);
        };

        widget.clearError = function () {
          clearMessage(notifier.types.ERROR);
        };

        widget.clearWarning = function () {
          clearMessage(notifier.types.WARNING);
        };

        widget.clearInfo = function () {
          clearMessage(notifier.types.INFO);
        };

        // Helper object for creating notifications
        function WidgetNotification() {
          var self = this;

          self.message = ko.observable('');
          self.style = ko.observable('inline');
          self.status = ko.observable('info');
          self.close = ko.observable(true);
          self.fade = ko.observable(false);

          self.display = function() {
            notifications.notify({
              'message' : self.message(),
              'style' : self.style(),
              'status' : self.status(),
              'id' : '#demoMessage',
              'close' : self.close(),
              'fade' : self.fade()
            });
          };

          self.clear = function() {
            notifications.clearNotifications('#demoMessage');
          };
        }
        widget.notification = ko.observable(new WidgetNotification());
      }
    };
  });
