/**
 * @fileoverview trackRecentlyViewed Widget.
 */
define(
  //-------------------------------------------------------------------
  // DEPENDENCIES
  //-------------------------------------------------------------------
  ['knockout', 'pubsub', 'storageApi'],

  //-------------------------------------------------------------------
  // MODULE DEFINITION
  //-------------------------------------------------------------------
  function(ko, pubsub, storageApi) {

    "use strict";

    var self;

    return{

      onLoad: function(widget) {
        self = widget;

        $.Topic(pubsub.topicNames.PRODUCT_VIEWED).subscribe(this.trackProductViewed);
      },

      trackProductViewed: function(product) {

        var viewHistoryLength = 12;

        // Retrieve array from localStorage (or create an empty array if not in localStorage)
        var recentlyViewedCookie = JSON.parse(storageApi.getInstance().getItem("cc.product.recentlyViewed")) || [];

        // See if the viewed product is already in the array, and remove it
        var foundIndex = -1;

        for (var i = 0; i < recentlyViewedCookie.length; i++) {
          if(recentlyViewedCookie[i].id == product.id()) {foundIndex = i; break;}
        }
        if (foundIndex > -1) recentlyViewedCookie.splice(foundIndex, 1);

        // Insert viewed product at top of array
        recentlyViewedCookie.unshift({
          "id": product.id(),
        });

        // Trim the array to the maximum length (+1 to cater for Product Details Page)
        if (recentlyViewedCookie.length > viewHistoryLength) {
          recentlyViewedCookie.length = viewHistoryLength + 1;
        }

        // Place array back into localStorage
        storageApi.getInstance().setItem("cc.product.recentlyViewed", JSON.stringify(recentlyViewedCookie));
      }
    };
  }
);
