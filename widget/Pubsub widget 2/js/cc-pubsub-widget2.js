define(
    ["knockout", "jquery", "pubsub"],
    function(ko, $, pubsub) {
        "use strict";
        return {
			resultMessage2: ko.observable(""),
			// Called when widget loads for first time only
            onLoad: function(widget) {
				console.log("Widget2 called onLoad");
				$.Topic("CUSTOM_TOPIC_1").subscribe(widget.recieveMessageFrom1.bind(widget));
			},
            // Called each time widget appears
			beforeAppear: function(page) {
				console.log("Widget2 called beforeAppear");
			},
			// Called when subsrcibed event fires
			recieveMessageFrom1: function(data){
				this.resultMessage2(data.message);
			},
			// Called when user clicks button
			publishEventFrom2: function(){
				console.log("validate widget 1 data here before checkout");
				var data = {message: "recieved from widget 2"};
				$.Topic("CUSTOM_TOPIC_2").publishWith(data, [{message: "validate widget 1"}]);
			}
		};
	}
);