define(

  //-------------------------------------------------------------------
  // DEPENDENCIES
  //-------------------------------------------------------------------
  ['knockout','jquery'],

  //-------------------------------------------------------------------
  // Module definition
  //-------------------------------------------------------------------


  function(ko) {
    'use strict';

    return {

      InputItem : ko.observable(""),
      SelectedItems : ko.observable(""),
      AllItems : ko.observableArray(["Observe","Think"]),

      onLoad : function(widget) {
        InputItem : ko.observable("<Enter your Text>");
  		},


     addedItem : function(){
        var widget = this;
        widget.AllItems.push(widget.InputItem());
        widget.InputItem("");
      },

      RemoveItem : function(){
         var widget = this;
         widget.AllItems.removeAll(widget.SelectedItems());
      },

      RemoveAllItem : function(){
         var widget = this;
         widget.AllItems.removeAll();
      }




}});
