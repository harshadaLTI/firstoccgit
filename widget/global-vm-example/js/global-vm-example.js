define(

  //-------------------------------------------------------------------
  // DEPENDENCIES
  //-------------------------------------------------------------------
  ['jquery', 'knockout', 'ccLogger', 'navigation', 'ccRestClient', 'notifier', 'ccKoExtensions'],

  //-------------------------------------------------------------------
  // Module definition
  //-------------------------------------------------------------------
  function ($, ko, ccLogger, navigation, ccRestClient, notifier) {

    'use strict';

    // Helper custom binding to add a locale variant of the current locale as the value for href on an anchor tag.
    ko.bindingHandlers.ccLocaleUrl = {
      init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
        var locale = ko.unwrap(valueAccessor());

        var localeUrl = navigation.getLocaleBasedUrl(locale).replace(navigation.getBaseURL(), '');

        $(element).attr('href', localeUrl);
      }
    };

    return {
      onLoad : function onLoad(widget) {

        widget.linksArray = ko.observableArray([]);

        widget.sortedLinks = ko.computed(function(){
          return widget.linksArray().sort(function(a, b){

            var nameA = a.displayName.toLowerCase();
            var nameB = b.displayName.toLowerCase();

            if (nameA < nameB) {
              return -1;
            }
            else if (nameA > nameB) {
              return 1;
            }
            else {
              return 0;
            }
          });
        });

        var links = [];

        for (var key in widget.links()) {
          var link = widget.links()[key];
          widget.linksArray.push(link);
        }

        widget.saveProfile = function() {
          var properties = widget.user().dynamicProperties();
          var data = {};

          for (var i=0;i<properties.length;i++) {
            var prop = properties[i];

            data[prop.id()] = prop.value();
          }

          ccRestClient.request(
            'updateProfile',
            data,
            function() {
              notifier.sendSuccess('global-vm-demo', 'Profile updated successfully.');
            },
            function() {
              notifier.sendError('global-vm-demo', 'Error updating profile.');
            }
          );
        };

        widget.customSettings = ko.observableArray([]);

        var customSettings = widget.site().extensionSiteSettings.customSettings;

        if (customSettings) {
          for (var settingKey in customSettings) {
            var prop = customSettings[settingKey];
            widget.customSettings.push({
              'label':settingKey,
              'value':prop
            });
          }
        }

        widget.markCartUpdated = function() {
          widget.cart().isDirty(true);
        };
      }
    };
  }
);
