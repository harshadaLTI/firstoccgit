<div id="global-vm-demoContainer">

  <h3>Global View Models</h3>
  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#site" aria-controls="site" role="tab" data-toggle="tab">Site</a></li>
    <li role="presentation"><a href="#cart" aria-controls="cart" role="tab" data-toggle="tab">Cart</a></li>
    <li role="presentation"><a href="#user" aria-controls="user" role="tab" data-toggle="tab">User</a></li>
    <li role="presentation"><a href="#links" aria-controls="links" role="tab" data-toggle="tab">Links</a></li>
    <li role="presentation"><a href="#locale" aria-controls="locale" role="tab" data-toggle="tab">Locale</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">

    <div role="tabpanel" class="tab-pane active" id="site">
      <!-- ko with:site -->
        <h3>Site Details</h3>
        <div class="row">
          <label class="col-sm-3">Site Name : </label>
          <div class="col-sm-7">
            <span data-bind="text:siteInfo.name"></span>
          </div>
        </div>
        <div class="row">
          <label class="col-sm-3">Supported Languages : </label>
          <div class="col-sm-7">
            <!-- ko foreach : additionalLanguages -->
              <!-- ko if:$index() > 0 -->
                <span> | </span>
              <!-- /ko -->
              <a data-bind="text:displayName, ccLocaleUrl : name"></a>
            <!-- /ko -->
          </div>
        </div>
        <div class="row">
          <label class="col-sm-3">Currency : </label>
          <div class="col-sm-7">
            <span data-bind="text:selectedPriceListGroup().currency.currencyCode + ' - ' + selectedPriceListGroup().currency.displayName"></span>
          </div>
        </div>
        <div class="row">
          <!-- ko with:$parent.customSettings -->
            <div class="col-sm-12">
              <!-- ko if:$data.length > 0 -->
                <h4>Custom Properties</h4>
                <form class="form-horizontal">
                  <!-- ko foreach:$data -->
                    <div class="form-group">
                      <label for="" class="col-sm-3" data-bind="text:label"></label>
                      <div class="col-sm-7">
                        <span data-bind="text:value"></span>
                      </div>
                    </div>
                  <!-- /ko -->
                </form>
              <!-- /ko -->
            </div>
          <!-- /ko -->
        </div>
      <!-- /ko -->
    </div>

    <div role="tabpanel" class="tab-pane" id="cart">
      <!-- ko with:cart -->
        <h3>Cart</h3>
        <div>
          <table class="table">
            <thead>
              <tr>
                <th>Display Name</th>
                <th>Quantity</th>
                <th>Price</th>
              </tr>
            </thead>
            <tbody>
              <!-- ko foreach:items -->
                <tr>
                  <!-- ko with:productData -->
                    <td data-bind="text: displayName"></td>
                  <!-- /ko -->
                  <td data-bind="text: quantity"></td>
                  <td data-bind="currency: {price: itemTotal(), currencyObj: $parents[1].site().selectedPriceListGroup().currency}"></td>
                </tr>
                <!-- ko foreach:discountInfo -->
                  <tr class="discountInfo">
                    <td colspan="2">

                    </td>
                    <td>
                      <span data-bind="text:promotionDesc"></span>
                    </td>
                  </tr>
                <!-- /ko -->
              <!-- /ko -->
            </tbody>
          </table>
          <div class="row">
            <div class="col-sm-12">
              <h4>Custom Properties</h4>
              <form class="form-horizontal">
                <!-- ko if:dynamicProperties -->
                <!-- ko foreach:dynamicProperties -->
                  <div data-bind="template: { name: 'dynamicProperty', data: $data }"></div>
                <!-- /ko -->
                <!-- /ko -->
                <div class="form-group">
                  <div class="col-sm-offset-3 col-sm-7">
                    <button class="btn btn-primary" data-bind="click:$parent.markCartUpdated">Update Cart</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      <!-- /ko -->
    </div>

    <div role="tabpanel" class="tab-pane" id="user">
      <!-- ko with:user -->
        <h3>User</h3>
        <div>
          <!-- ko if:loggedIn -->
            <div data-bind="template: { name: 'userDetails', data: $data }"></div>
            <div data-bind="template: { name: 'userDynamicProperties', data: $data }"></div>
          <!-- /ko -->
          <!-- ko if:!loggedIn() -->
            <p>Anonymous</p>
          <!-- /ko -->
        </div>
      <!-- /ko -->
    </div>

    <div role="tabpanel" class="tab-pane links" id="links">
      <h3>Links</h3>
      <div class="linksContainer">
      <!-- ko foreach:sortedLinks -->
        <a data-bind="ccLink:$data"></a>
      <!-- /ko -->
      </div>
    </div>

    <div role="tabpanel" class="tab-pane links" id="locale">
      <h3>Locale</h3>
      <!-- ko with:locale -->
        <label>Current Locale : </label>
        <span data-bind="text:$data"></span>
      <!-- /ko -->
    </div>

    <script type='text/html' id='userDetails'>
      <div class="row">
        <label class="col-sm-3">First Name : </label>
        <div class="col-sm-7">
          <span data-bind="text:firstName"></span>
        </div>
      </div>
      <div class="row">
        <label class="col-sm-3">Last Name : </label>
        <div class="col-sm-7">
          <span data-bind="text:lastName"></span>
        </div>
      </div>
      <div class="row">
        <label class="col-sm-3">Email Address : </label>
        <div class="col-sm-7">
          <span data-bind="text:email"></span>
        </div>
      </div>
      <div class="row">
        <label class="col-sm-3">Preferred Locale : </label>
        <div class="col-sm-7">
          <span data-bind="text:locale"></span>
        </div>
      </div>
      <div class="row">
        <label class="col-sm-3">Receive Marketing Emails : </label>
        <div class="col-sm-7">
          <span data-bind="text:receiveEmail"></span>
        </div>
      </div>
    </script>

    <script type='text/html' id='userDynamicProperties'>
      <h4>Custom Properties</h4>
      <form class="form-horizontal">
        <!-- ko if:dynamicProperties -->
        <!-- ko foreach:dynamicProperties -->
          <div data-bind="template: { name: 'dynamicProperty', data: $data }"></div>
        <!-- /ko -->
        <!-- /ko -->
        <div class="form-group">
          <div class="col-sm-offset-3 col-sm-7">
            <button class="btn btn-primary" data-bind="click:$parents[1].saveProfile">Save</button>
          </div>
        </div>
      </form>
    </script>

    <script type="text/html" id="dynamicProperty">
      <div class="form-group">
        <label for="" class="col-sm-3" data-bind="text:label"></label>
        <div class="col-sm-4">
          <input type="text" class="form-control" data-bind="value:value" />
        </div>
      </div>
    </script>
  </div>

</div>
