/**
 * @fileoverview Sample Widget.
 *
 * @author Your Name Here
 */
define(	
	//-------------------------------------------------------------------
	// DEPENDENCIES
	//-------------------------------------------------------------------
	['knockout'],

	//-------------------------------------------------------------------
	// MODULE DEFINITION
	//-------------------------------------------------------------------
	function (ko) {

    "use strict";

	
	
    return {
		
		isMorning : ko.observable(false),
		
		itemToAdd:ko.observable(""),
		allItems:ko.observableArray(["Mobiles", "Chargers", "OTG"]),// Initial items
		selectedItems :ko.observableArray(["Covers"]),

		
		
		
		onLoad: function(widget) {	
			var self=this;
			widget.addItem = function () {
              if ((this.itemToAdd() != "") && (this.allItems.indexOf(this.itemToAdd()) < 0)) // Prevent blanks and duplicates
              this.allItems.push(this.itemToAdd());
              this.itemToAdd(""); // Clear the text box
			};
			
			widget.removeSelected = function () {
				this.allItems.removeAll(this.selectedItems());
				this.selectedItems([]); // Clear selection
			};
			


		
			
		},
		
		beforeAppear:function(page){
			var widget = this;	
			var date=new Date();
			if (date.getHours()<12){
				this.isMorning(true);
			}
			else{
				this.isMorning(false);
			}
		  
		}
    };
  }
);
